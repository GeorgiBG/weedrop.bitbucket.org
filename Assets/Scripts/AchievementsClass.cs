﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AchievementsClass : MonoBehaviour 
{
    [SerializeField]
    GameObject AchievementsPanel;

    [SerializeField]
    GameObject HomeItemsPanel;

    [SerializeField]
    GameObject TipOfDayPanel;
    [SerializeField]
    GameObject ProfilePanel;
    [SerializeField]
    GameObject EncyclopediaPanel;
    [SerializeField]
    GameObject SettingsPanel;

    [SerializeField]
    Text TitleTxt;

    string AppHeaderStr = AppManagerClass.applicationTitle;

    bool isAchievementsPanelActive = false;

    bool achievementFlag;

    [SerializeField]
    List<string> achievements;

    [SerializeField]
    Text achievementContent;

    string tempAchievs = "";

    int counter = 0;

    [SerializeField]
    Button achievementsHeaderBtn;

    [SerializeField]
    GameObject AchievementsGlobalHolder;
    bool achievementGlobal;

    [SerializeField]
    float achievemntTimerConstant;
    float achievemntTimer;

    // Use this for initialization
    void Awake()
    {
        achievementFlag = true;
        // 1. day 1 = newcomer
        // 2. day 2 = consistency
        // 3. logged water amount - 1l
        // 4. logged water amount - 10l
        // 5. logged water amount - 100l
        // 6. logged water amount - 1000l
    }

    void Update()
    {
        // REALLY LAGGY, QUICK, BASIC LEVEL PROGRESSION SYSTEM INTRODUCED (JUST VISUALLY)
        if (achievementsHeaderBtn != null)
        { 
            if(counter == 0)
                achievementsHeaderBtn.GetComponentInChildren<Text>().text = "Level: 0";
            else if(counter > 0 && counter < 3)
                achievementsHeaderBtn.GetComponentInChildren<Text>().text = "Level: 1";
            else if (counter >= 3 && counter < 6)
                achievementsHeaderBtn.GetComponentInChildren<Text>().text = "Level: 2";
            else
                achievementsHeaderBtn.GetComponentInChildren<Text>().text = "Level: 3";
        }

        if(achievementGlobal)
        {
			AchievementsGlobalHolder.SetActive(true);
		
			achievemntTimer -= Time.deltaTime;   
			if (achievemntTimer <= 0)
			{
				achievementGlobal = false;
				achievemntTimer = achievemntTimerConstant;
			}
        }

        if(!achievementGlobal)
            AchievementsGlobalHolder.SetActive(false);

        HandleAchievements();

        while(true)
        {
            if (achievementFlag)
            {
                achievementContent.text = tempAchievs;
                achievementFlag = false;
            }

            if (achievementFlag == false)
                break;
        }
    }

    private void HandleAchievements()
    {
        AppManagerClass.LoadPrefs();

        if (AppManagerClass.day >= 1)
            if (!achievementContent.text.Contains(achievements[0] + "\n"))
            {
                achievementGlobal = true;
                AchievementsGlobalHolder.GetComponentInChildren<Text>().text = "(Achievement)\n\n" + achievements[0];

                counter++;
                tempAchievs += counter + ". " + achievements[0] + "\n";
                achievementFlag = true;
            }

        if (AppManagerClass.day >= 2)
            if (!achievementContent.text.Contains(achievements[1] + "\n"))
            {
                achievementGlobal = true;
                AchievementsGlobalHolder.GetComponentInChildren<Text>().text = "(Achievement)\n\n" + achievements[1];

                counter++;
                tempAchievs += counter + ". " + achievements[1] + "\n";
                achievementFlag = true;
            }

        if (AppManagerClass.loggedWaterQuantity >= 100.0f)
            if (!achievementContent.text.Contains(achievements[2] + "\n"))
            {
                achievementGlobal = true;
                AchievementsGlobalHolder.GetComponentInChildren<Text>().text = "(Achievement)\n\n" + achievements[2];

                counter++;
                tempAchievs += counter + ". " + achievements[2] + "\n";
                achievementFlag = true;
            }

        if (AppManagerClass.loggedWaterQuantity >= 1000.0f)
            if (!achievementContent.text.Contains(achievements[3] + "\n"))
            {
                achievementGlobal = true;
                AchievementsGlobalHolder.GetComponentInChildren<Text>().text = "(Achievement)\n\n" + achievements[3];

                counter++;
                tempAchievs += counter + ". " + achievements[3] + "\n";
                achievementFlag = true;
            }

        if (AppManagerClass.loggedWaterQuantity >= 10000.0f)
            if (!achievementContent.text.Contains(achievements[4] + "\n"))
            {
                achievementGlobal = true;
                AchievementsGlobalHolder.GetComponentInChildren<Text>().text = "(Achievement)\n\n" + achievements[4];

                counter++;
                tempAchievs += counter + ". " + achievements[4] + "\n";
                achievementFlag = true;
            }

        if (AppManagerClass.loggedWaterQuantity >= 100000.0f)
            if (!achievementContent.text.Contains(achievements[5] + "\n"))
            {
                achievementGlobal = true;
                AchievementsGlobalHolder.GetComponentInChildren<Text>().text = "(Achievement)\n\n" + achievements[5];

                counter++;
                tempAchievs += counter + ". " + achievements[5] + "\n";
                achievementFlag = true;
            }
    }

    public void ToggleTipsOfDayPanel()
    {
        isAchievementsPanelActive = !isAchievementsPanelActive;

        if (isAchievementsPanelActive)
            ShowAchievementsPanel();
        else
            HideAchievementsPanel();
    }

    private void ShowAchievementsPanel()
    {
        HomeItemsPanel.SetActive(false);
        TipOfDayPanel.SetActive(false);
        ProfilePanel.SetActive(false);
        EncyclopediaPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        AchievementsPanel.SetActive(true);

        TitleTxt.text = "Achievements";
    }

    private void HideAchievementsPanel()
    {
        AchievementsPanel.SetActive(false);
        TipOfDayPanel.SetActive(false);
        ProfilePanel.SetActive(false);
        EncyclopediaPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        HomeItemsPanel.SetActive(true);

        TitleTxt.text = AppHeaderStr;
    }
}