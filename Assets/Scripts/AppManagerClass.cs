﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AppManagerClass : MonoBehaviour
{
    [SerializeField]
    bool deleteSavedData;

    [SerializeField]
    Text initialPageTitleTxt;

    public static string applicationTitle = "Wee Drop";
    public static float overallWaterQuantity;
    public static float maxWaterQuantity;
    public static float waterFillAmount;
    public static bool firstLaunch;
    public static string dailyActivity;
    public static int day;
    public static bool viewedDayTip;
    public static string savedDate;
    public static float loggedWaterQuantity = 0;

    [SerializeField]
    InputField InputName;
    public static string name;

    [SerializeField]
    InputField InputWeight;
    public static float weight;

    void Awake()
    {
        if (initialPageTitleTxt != null)
            initialPageTitleTxt.text = applicationTitle;

        if(deleteSavedData)
            PlayerPrefs.DeleteAll();

        if (PlayerPrefs.HasKey("firstLaunch"))
            LoadPrefs();
        else
            firstLaunch = true;

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        InputWeight.text = weight.ToString();
    }

	// Update is called once per frame
	void Update()
    {
        if (!firstLaunch)
            if (!SceneManager.GetActiveScene().name.Equals("Main"))
                SceneManager.LoadScene("Main");

        if(InputWeight != null)
            if (InputWeight.text != weight.ToString())
            {
                weight = float.Parse(InputWeight.text);
                InputWeight.text = weight.ToString();
            }
	}

    public static void SavePrefs(bool firstLaunch, string username, float weight, string activityLevel, 
    float waterDrank, float waterToDrink, float waterFillAmount, int dayCount, bool viewedTip, float loggedWater)
    {
        PlayerPrefs.SetInt("firstLaunch", Convert.ToInt32(firstLaunch));
        PlayerPrefs.SetString("username", username);
        PlayerPrefs.SetFloat("weight", weight);
        PlayerPrefs.SetString("activityLevel", activityLevel);
        PlayerPrefs.SetFloat("waterDrank", overallWaterQuantity);
        PlayerPrefs.SetFloat("waterToDrink", maxWaterQuantity);
        PlayerPrefs.SetFloat("loggedWater", loggedWater);
        PlayerPrefs.SetFloat("waterFillAmount", waterFillAmount);
        PlayerPrefs.SetInt("dayCount", dayCount);
        PlayerPrefs.SetInt("viewedTip", Convert.ToInt32(viewedTip));
        
        PlayerPrefs.Save();
    }

    public static void LoadPrefs()
    {
        firstLaunch = Convert.ToBoolean(PlayerPrefs.GetInt("firstLaunch"));
        name = PlayerPrefs.GetString("username");
        weight = PlayerPrefs.GetFloat("weight");
        dailyActivity = PlayerPrefs.GetString("activityLevel");
        overallWaterQuantity = PlayerPrefs.GetFloat("waterDrank");
        maxWaterQuantity = PlayerPrefs.GetFloat("waterToDrink");
        loggedWaterQuantity = PlayerPrefs.GetFloat("loggedWater");
        waterFillAmount = PlayerPrefs.GetFloat("waterFillAmount");
        day = PlayerPrefs.GetInt("dayCount");
        viewedDayTip = Convert.ToBoolean(PlayerPrefs.GetInt("viewedTip"));
    }

    public static void SaveDate(string date)
    {
        PlayerPrefs.SetString("date", date);
        PlayerPrefs.Save();
    }

    public static void LoadDate()
    {
        savedDate = PlayerPrefs.GetString("date");
    }

    public void IncreaseWeightDetails()
    {
        weight = float.Parse(InputWeight.text) + 1;
        InputWeight.text = weight.ToString();
    }

    public void DecraseWeightDetails()
    {
        weight = float.Parse(InputWeight.text) - 1;
        InputWeight.text = weight.ToString();
    }

    public void Continue()
    {
        if (firstLaunch)
            if (!InputName.text.Equals(""))
            {
                name = InputName.text;
                weight = float.Parse(InputWeight.text);
                firstLaunch = false;

                overallWaterQuantity = 0.0f;
                maxWaterQuantity = 0.019f * (weight * 2.2f) * 1000.0f; // * 2.2 because calculating in pounds
                loggedWaterQuantity = 0.0f;
                waterFillAmount = 0.0f;
                dailyActivity = "light";
                day = 1;
                SavePrefs(firstLaunch, name, weight, dailyActivity, overallWaterQuantity, maxWaterQuantity, waterFillAmount, day, viewedDayTip, loggedWaterQuantity);

                savedDate = DateTime.Today.ToShortDateString();
                SaveDate(savedDate);

                // Load Details
                LoadPrefs();
                // Go to Application's Main Scene
                SceneManager.LoadScene("Main");
            }
    }
}