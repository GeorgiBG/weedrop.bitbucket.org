﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class DropClass : MonoBehaviour
{
    [SerializeField]
    Text TitleTxt;

    [SerializeField]
    Image dropScroll;
    [SerializeField]
    Image waterBarScroll;

    [SerializeField]
    GameObject InputWaterDetailsPanel;

    public InputField waterQuantityInputField;
    
    public Text waterIndicatorTxt;
    Color waterIndicatorBaseColor;

    float quantity = 0.0f;

    bool isAddWaterPanelActive = false;

    [SerializeField]
    GameObject ManualInputWaterPanel;

    void Awake()
    {
        /* Load Save Data */
        AppManagerClass.LoadDate();
        AppManagerClass.LoadPrefs();

        Debug.Log("Day: " + AppManagerClass.day);

        SetInitialReferences();
    }

	// Use this for initialization
	void Start ()
    {
        if (AppManagerClass.firstLaunch)
            ResetStats();
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Drop Fill Amount Check/Set
        if(dropScroll.fillAmount != (AppManagerClass.overallWaterQuantity / AppManagerClass.maxWaterQuantity))
            dropScroll.fillAmount = AppManagerClass.overallWaterQuantity / AppManagerClass.maxWaterQuantity;
        // Water Bar Fill Amount Check/Set
        if (waterBarScroll.fillAmount != (AppManagerClass.overallWaterQuantity / AppManagerClass.maxWaterQuantity))
            waterBarScroll.fillAmount = AppManagerClass.overallWaterQuantity / AppManagerClass.maxWaterQuantity;

        /* RESET STATS FOR THE NEW DAY (12AM) */
        if(AppManagerClass.savedDate != DateTime.Today.ToShortDateString())
        {
            AppManagerClass.SaveDate(DateTime.Today.ToShortDateString());
            AppManagerClass.LoadDate();

            AppManagerClass.overallWaterQuantity = 0.0f;
            dropScroll.fillAmount = 0.0f;
            waterBarScroll.fillAmount = 0.0f;
            AppManagerClass.day += 1;
            AppManagerClass.viewedDayTip = false;

            // Save Details
            AppManagerClass.SavePrefs(AppManagerClass.firstLaunch, AppManagerClass.name, AppManagerClass.weight, AppManagerClass.dailyActivity,
            AppManagerClass.overallWaterQuantity, AppManagerClass.maxWaterQuantity, AppManagerClass.waterFillAmount, AppManagerClass.day,
            AppManagerClass.viewedDayTip, AppManagerClass.loggedWaterQuantity);

            // Load New Details
            AppManagerClass.LoadPrefs();
        }
        /* END OF STATS RESETTING FOR NEW DAY */

        if (AppManagerClass.overallWaterQuantity > AppManagerClass.maxWaterQuantity / 2)
        {
            waterIndicatorTxt.GetComponent<Text>().color = Color.white;
            Debug.LogWarning("Halved.");
        }

        if (AppManagerClass.overallWaterQuantity > AppManagerClass.maxWaterQuantity)
        {
            waterIndicatorTxt.GetComponent<Text>().color = Color.yellow;
            Debug.LogWarning("Exceeded.");
        }

        waterIndicatorTxt.text = AppManagerClass.overallWaterQuantity.ToString() + " / " + Mathf.Round(AppManagerClass.maxWaterQuantity).ToString();
	}

    private void SetInitialReferences()
    {
        // Set initials...
        if (this.TitleTxt != null)
            this.TitleTxt.text = AppManagerClass.applicationTitle;
        waterQuantityInputField.text = quantity.ToString();
        dropScroll.fillAmount = AppManagerClass.waterFillAmount;
        waterBarScroll.fillAmount = AppManagerClass.waterFillAmount;
    }

    private void ResetStats()
    {
        dropScroll.fillAmount = 0.0f;
        waterBarScroll.fillAmount = 0.0f;
    }

    public void ToggleInputArea()
    {
        isAddWaterPanelActive = !isAddWaterPanelActive;

        if (isAddWaterPanelActive)
        {
            ShowWaterDataInputArea();
            HideManualInputWaterPanel();            
        }            
        else if(!isAddWaterPanelActive)
        {
            HideWaterDataInputArea();
            HideManualInputWaterPanel();
        }
    }

    public void ShowManualInputWaterPanel()
    {
        ManualInputWaterPanel.SetActive(true);
        HideWaterDataInputArea();
    }

    public void HideManualInputWaterPanel()
    {
        ManualInputWaterPanel.SetActive(false);
    }

    private void ShowWaterDataInputArea()
    {
        InputWaterDetailsPanel.SetActive(true);
    }

    private void HideWaterDataInputArea()
    {
        InputWaterDetailsPanel.SetActive(false);
    }

    public void IncreaseDetails(float amount)
    {
        quantity = float.Parse(waterQuantityInputField.text) + amount; // 10.0f
                
        // PRINT ML
        waterQuantityInputField.text = quantity.ToString();
    }

    public void DecraseDetails(float amount)
    {
        if (quantity >= amount)
            quantity = float.Parse(waterQuantityInputField.text) - amount; // 10.0f
        else
            Debug.LogWarning("Cannot decrease!!!");

        // PRINT ML
        waterQuantityInputField.text = quantity.ToString();
    }

    public void SetWaterDetails(float amount)
    {
        if (waterQuantityInputField.isActiveAndEnabled)
        {
            quantity = float.Parse(waterQuantityInputField.text);
            AppManagerClass.overallWaterQuantity += quantity;
            AppManagerClass.loggedWaterQuantity += quantity;
        }
        else
        {
            AppManagerClass.overallWaterQuantity += amount;
            AppManagerClass.loggedWaterQuantity += amount;
        }

        AppManagerClass.waterFillAmount = AppManagerClass.overallWaterQuantity / AppManagerClass.maxWaterQuantity;

        dropScroll.fillAmount = AppManagerClass.waterFillAmount;
        waterBarScroll.fillAmount = AppManagerClass.waterFillAmount;

        // Save Details
        AppManagerClass.SavePrefs(AppManagerClass.firstLaunch, AppManagerClass.name, AppManagerClass.weight, AppManagerClass.dailyActivity,
        AppManagerClass.overallWaterQuantity, AppManagerClass.maxWaterQuantity, AppManagerClass.waterFillAmount, AppManagerClass.day, 
        AppManagerClass.viewedDayTip, AppManagerClass.loggedWaterQuantity);

        // Load New Details
        AppManagerClass.LoadPrefs();

        quantity = 0;
        waterQuantityInputField.text = quantity.ToString();

        HideManualInputWaterPanel();
        HideWaterDataInputArea();
    }
}