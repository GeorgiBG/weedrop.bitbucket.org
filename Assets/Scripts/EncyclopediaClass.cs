﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class EncyclopediaClass : MonoBehaviour 
{
    [SerializeField]
    GameObject EncyclopediaPanel;

    [SerializeField]
    GameObject HomeItemsPanel;

    [SerializeField]
    GameObject AchievementsPanel;
    [SerializeField]
    GameObject TipOfDayPanel;
    [SerializeField]
    GameObject ProfilePanel;
    [SerializeField]
    GameObject SettingsPanel;

    [SerializeField]
    Text TitleTxt;

    string AppHeaderStr = AppManagerClass.applicationTitle;

    bool isEncyclopediaPanelActive = false;

    // Use this for initialization
    void Awake()
    {
    }

    public void ToggleEncyclopediaPanel()
    {
        isEncyclopediaPanelActive = !isEncyclopediaPanelActive;

        if (isEncyclopediaPanelActive)
            ShowEncyclopediaPanel();
        else
            HideEncyclopediaPanel();
    }

    private void ShowEncyclopediaPanel()
    {
        HomeItemsPanel.SetActive(false);
        AchievementsPanel.SetActive(false);
        TipOfDayPanel.SetActive(false);
        ProfilePanel.SetActive(false);
        SettingsPanel.SetActive(false);
        EncyclopediaPanel.SetActive(true);

        TitleTxt.text = "Achievements";
    }

    private void HideEncyclopediaPanel()
    {
        EncyclopediaPanel.SetActive(false);
        AchievementsPanel.SetActive(false);
        TipOfDayPanel.SetActive(false);
        ProfilePanel.SetActive(false);
        SettingsPanel.SetActive(false);
        HomeItemsPanel.SetActive(true);

        TitleTxt.text = AppHeaderStr;
    }
}
