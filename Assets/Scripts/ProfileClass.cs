﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class ProfileClass : MonoBehaviour 
{
    [SerializeField]
    GameObject ProfilePanel;

    [SerializeField]
    GameObject HomeItemsPanel;

    [SerializeField]
    GameObject AchievementsPanel;
    [SerializeField]
    GameObject TipOfDayPanel;
    [SerializeField]
    GameObject EncyclopediaPanel;
    [SerializeField]
    GameObject SettingsPanel;

    [SerializeField]
    Text TitleTxt;

    [SerializeField]
    InputField NameTxt;
    [SerializeField]
    InputField WeightTxt;

    [SerializeField]
    Button sedentaryBtn;
    [SerializeField]
    Button lightBtn;
    [SerializeField]
    Button activeBtn;
    [SerializeField]
    Button veryActiveBtn;

    string AppHeaderStr = AppManagerClass.applicationTitle;

    bool isProfilePanelActive = false;
    bool isSaveBtnClicked = false;

    float activityConstant = 0.0f;

	// Use this for initialization
    void Awake()
    {
        NameTxt.text = AppManagerClass.name;
        WeightTxt.text = AppManagerClass.weight.ToString();
        
        if(AppManagerClass.dailyActivity.Equals("sedentary"))
        {
            sedentaryBtn.GetComponent<Image>().color = Color.gray;
            sedentaryBtn.GetComponentInChildren<Text>().color = Color.white;
        }
        else if (AppManagerClass.dailyActivity.Equals("light"))
        {
            lightBtn.GetComponent<Image>().color = Color.gray;
            lightBtn.GetComponentInChildren<Text>().color = Color.white;
        }
        else if (AppManagerClass.dailyActivity.Equals("active"))
        {
            activeBtn.GetComponent<Image>().color = Color.gray;
            activeBtn.GetComponentInChildren<Text>().color = Color.white;
        }
        else if (AppManagerClass.dailyActivity.Equals("very active"))
        {
            veryActiveBtn.GetComponent<Image>().color = Color.gray;
            veryActiveBtn.GetComponentInChildren<Text>().color = Color.white;
        }
    }

    void Update()
    {
        if (WeightTxt != null)
            if (WeightTxt.text != AppManagerClass.weight.ToString())
            {
                AppManagerClass.weight = float.Parse(WeightTxt.text);
                WeightTxt.text = AppManagerClass.weight.ToString();
            }
    }

    public void ToggleProfilePanel()
    {
        isProfilePanelActive = !isProfilePanelActive;

        if (isProfilePanelActive)
            ShowProfilePanel();
        else
            HideProfilePanel();
    }

    private void ShowProfilePanel()
    {
        HomeItemsPanel.SetActive(false);
        AchievementsPanel.SetActive(false);
        TipOfDayPanel.SetActive(false);
        EncyclopediaPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        ProfilePanel.SetActive(true);
        
        TitleTxt.text = "Profile";
    }

    private void HideProfilePanel()
    {
        ProfilePanel.SetActive(false);
        AchievementsPanel.SetActive(false);
        TipOfDayPanel.SetActive(false);
        EncyclopediaPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        HomeItemsPanel.SetActive(true);
        
        TitleTxt.text = AppHeaderStr;

        if (!isSaveBtnClicked && NameTxt.text != AppManagerClass.name)
            NameTxt.text = AppManagerClass.name;
    }

    public void IncreaseWeightDetails()
    {
        AppManagerClass.weight = float.Parse(WeightTxt.text) + 1;
        WeightTxt.text = AppManagerClass.weight.ToString();
    }

    public void DecraseWeightDetails()
    {
        AppManagerClass.weight = float.Parse(WeightTxt.text) - 1;
        WeightTxt.text = AppManagerClass.weight.ToString();
    }

    public void ChooseDailyActivity(string activityLevel)
    {
        if(activityLevel.Equals("sedentary"))
        {
            sedentaryBtn.GetComponent<Image>().color = Color.gray;
            sedentaryBtn.GetComponentInChildren<Text>().color = Color.white;
            
            lightBtn.GetComponent<Image>().color = Color.white;
            lightBtn.GetComponentInChildren<Text>().color = Color.gray;
            
            activeBtn.GetComponent<Image>().color = Color.white;
            activeBtn.GetComponentInChildren<Text>().color = Color.gray;
            
            veryActiveBtn.GetComponent<Image>().color = Color.white;
            veryActiveBtn.GetComponentInChildren<Text>().color = Color.gray;

            AppManagerClass.dailyActivity = activityLevel;
            activityConstant = 0.018f;
        }
        else if(activityLevel.Equals("light"))
        {
            lightBtn.GetComponent<Image>().color = Color.gray;
            lightBtn.GetComponentInChildren<Text>().color = Color.white;

            sedentaryBtn.GetComponent<Image>().color = Color.white;
            sedentaryBtn.GetComponentInChildren<Text>().color = Color.gray;

            activeBtn.GetComponent<Image>().color = Color.white;
            activeBtn.GetComponentInChildren<Text>().color = Color.gray;

            veryActiveBtn.GetComponent<Image>().color = Color.white;
            veryActiveBtn.GetComponentInChildren<Text>().color = Color.gray;
            
            AppManagerClass.dailyActivity = activityLevel;
            activityConstant = 0.019f;
        }
        else if(activityLevel.Equals("active"))
        {
            activeBtn.GetComponent<Image>().color = Color.gray;
            activeBtn.GetComponentInChildren<Text>().color = Color.white;

            sedentaryBtn.GetComponent<Image>().color = Color.white;
            sedentaryBtn.GetComponentInChildren<Text>().color = Color.gray;

            lightBtn.GetComponent<Image>().color = Color.white;
            lightBtn.GetComponentInChildren<Text>().color = Color.gray;

            veryActiveBtn.GetComponent<Image>().color = Color.white;
            veryActiveBtn.GetComponentInChildren<Text>().color = Color.gray;

            AppManagerClass.dailyActivity = activityLevel;
            activityConstant = 0.021f;
        }
        else if(activityLevel.Equals("very active"))
        {
            veryActiveBtn.GetComponent<Image>().color = Color.gray;
            veryActiveBtn.GetComponentInChildren<Text>().color = Color.white;

            sedentaryBtn.GetComponent<Image>().color = Color.white;
            sedentaryBtn.GetComponentInChildren<Text>().color = Color.gray;

            lightBtn.GetComponent<Image>().color = Color.white;
            lightBtn.GetComponentInChildren<Text>().color = Color.gray;

            activeBtn.GetComponent<Image>().color = Color.white;
            activeBtn.GetComponentInChildren<Text>().color = Color.gray;

            AppManagerClass.dailyActivity = activityLevel;
            activityConstant = 0.022f;
        }
    }


    public void SaveProfileDetails()
    {
        isSaveBtnClicked = true;

        AppManagerClass.name = NameTxt.text;
        AppManagerClass.weight = float.Parse(WeightTxt.text);

        AppManagerClass.maxWaterQuantity = activityConstant * (AppManagerClass.weight * 2.2f) * 1000.0f;

        if(activityConstant == 0.018f)
            AppManagerClass.dailyActivity = "sedentary";

        if (activityConstant == 0.019f)
            AppManagerClass.dailyActivity = "light";

        if (activityConstant == 0.021f)
            AppManagerClass.dailyActivity = "active";

        if (activityConstant == 0.022f)
            AppManagerClass.dailyActivity = "very active";

        // Save Details
        AppManagerClass.SavePrefs(AppManagerClass.firstLaunch, AppManagerClass.name, AppManagerClass.weight, AppManagerClass.dailyActivity,
        AppManagerClass.overallWaterQuantity, AppManagerClass.maxWaterQuantity, AppManagerClass.waterFillAmount, AppManagerClass.day,
        AppManagerClass.viewedDayTip, AppManagerClass.loggedWaterQuantity);

        // Load New Details
        AppManagerClass.LoadPrefs();

        // Deal with UI...
        TitleTxt.text = AppManagerClass.applicationTitle;
        ProfilePanel.SetActive(false);
        HomeItemsPanel.SetActive(true);

        isSaveBtnClicked = false;
    }
}