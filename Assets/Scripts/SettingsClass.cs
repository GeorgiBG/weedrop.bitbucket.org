﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class SettingsClass : MonoBehaviour
{
    [SerializeField]
    GameObject SettingsPanel;

    [SerializeField]
    GameObject AchievementsPanel;
    [SerializeField]
    GameObject TipOfDayPanel;
    [SerializeField]
    GameObject ProfilePanel;
    [SerializeField]
    GameObject EncyclopediaPanel;

    [SerializeField]
    GameObject HomeItemsPanel;

    [SerializeField]
    Text TitleTxt;

    string AppHeaderStr = AppManagerClass.applicationTitle;

    bool isSettingsPanelActive = false;

    // Use this for initialization
    void Awake()
    {

    }

    public void ToggleSettingsPanel()
    {
        isSettingsPanelActive = !isSettingsPanelActive;

        if (isSettingsPanelActive)
            ShowSettingsPanel();
        else
            HideSettingsPanel();
    }

    private void ShowSettingsPanel()
    {
        HomeItemsPanel.SetActive(false);
        AchievementsPanel.SetActive(false);
        TipOfDayPanel.SetActive(false);
        ProfilePanel.SetActive(false);
        EncyclopediaPanel.SetActive(false);
        SettingsPanel.SetActive(true);

        TitleTxt.text = "Achievements";
    }

    private void HideSettingsPanel()
    {
        SettingsPanel.SetActive(false);
        AchievementsPanel.SetActive(false);
        TipOfDayPanel.SetActive(false);
        ProfilePanel.SetActive(false);
        EncyclopediaPanel.SetActive(false);
        HomeItemsPanel.SetActive(true);

        TitleTxt.text = AppHeaderStr;
    }

    public void GoToAboutPage()
    {
        Application.OpenURL("http://georgim.com/system/water-app/");
    }
}
