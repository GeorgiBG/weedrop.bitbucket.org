﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TipsOfDayClass : MonoBehaviour
{
    [SerializeField]
    GameObject TipsOfDayPanel;

    [SerializeField]
    GameObject HomeItemsPanel;

    [SerializeField]
    GameObject AchievementsPanel;
    [SerializeField]
    GameObject ProfilePanel;
    [SerializeField]
    GameObject EncyclopediaPanel;
    [SerializeField]
    GameObject SettingsPanel;

    [SerializeField]
    Text TitleTxt;

    string AppHeaderStr = AppManagerClass.applicationTitle;

    bool isTipsOfDayPanelActive = false;

    [SerializeField]
    Text archiveContent;

    [SerializeField]
    Text dayTipContent;

    [SerializeField]
    Text dayPanelTitleTxt;

    [SerializeField]
    Button dayTipBtn;

    [SerializeField]
    List<string> tips;

    string currTip;

    bool archiveFlag;

    // Use this for initialization
    void Awake()
    {
        archiveFlag = true;
    }

    void Update()
    {
        AppManagerClass.LoadPrefs();

        dayPanelTitleTxt.text = "Day " + AppManagerClass.day;

        if (AppManagerClass.viewedDayTip)
        {
            // Set button interactability to false
            dayTipBtn.GetComponent<Button>().interactable = false;

            // get tip
            FindCurrentDayTip();
            // display tip
            ShowCurrentDayTip();
        }
        else
        {
			dayTipBtn.GetComponent<Button>().interactable = true;
			dayTipBtn.GetComponentInChildren<Text>().text = "Today's Tip ?";
        }
    }

    public void GetTodaysTip()
    {
        // Set viewed flag to true
        AppManagerClass.viewedDayTip = true;

        // Save Details
        AppManagerClass.SavePrefs(AppManagerClass.firstLaunch, AppManagerClass.name, AppManagerClass.weight, AppManagerClass.dailyActivity,
        AppManagerClass.overallWaterQuantity, AppManagerClass.maxWaterQuantity, AppManagerClass.waterFillAmount, AppManagerClass.day, 
        AppManagerClass.viewedDayTip, AppManagerClass.loggedWaterQuantity);

        // Load New Details
        AppManagerClass.LoadPrefs();
    }

    void FindCurrentDayTip()
    {
        for (var i = 0; i < AppManagerClass.day; ++i)
            currTip = tips[AppManagerClass.day - 1];
    }

    void ShowCurrentDayTip()
    {
        dayTipContent.text = currTip;
    }

    void ShowArchivedTips()
    {
        while (true)
        {
            if (archiveFlag)
            {
                if (AppManagerClass.day != 1)
                    archiveContent.text += "\n" + currTip;
                else
                    archiveContent.text += currTip;

                archiveFlag = false;
            }

            if (archiveFlag == false)
                break;
        }
    }

    public void ToggleTipsOfDayPanel()
    {
        isTipsOfDayPanelActive = !isTipsOfDayPanelActive;

        if (isTipsOfDayPanelActive)
            ShowTipsOfDayPanel();
        else
            HideTipsOfDayPanel();
    }

    private void ShowTipsOfDayPanel()
    {
        HomeItemsPanel.SetActive(false);
        AchievementsPanel.SetActive(false);
        ProfilePanel.SetActive(false);
        EncyclopediaPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        TipsOfDayPanel.SetActive(true);

        TitleTxt.text = "Tip of the Day";
    }

    private void HideTipsOfDayPanel()
    {
        TipsOfDayPanel.SetActive(false);
        AchievementsPanel.SetActive(false);
        ProfilePanel.SetActive(false);
        EncyclopediaPanel.SetActive(false);
        SettingsPanel.SetActive(false);
        HomeItemsPanel.SetActive(true);

        TitleTxt.text = AppHeaderStr;
    }
}